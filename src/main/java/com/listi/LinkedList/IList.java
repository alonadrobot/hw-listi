package com.listi.LinkedList;

public interface IList<T> {
        void clear();
        T size();
        T get(int index);
        boolean add(T value);
        boolean add(int index, T value);
        boolean remove(T value);
        T removeByIndex(int index);
        boolean contains(T value);
        boolean set(int index, T value);
        String print();
        T[] toArray();
        boolean removeAll(T[] ar);
}
