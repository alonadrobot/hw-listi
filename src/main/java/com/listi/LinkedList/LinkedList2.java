package com.listi.LinkedList;

import com.listi.LinkedList.IList;

public class LinkedList2 implements IList {
    class Node{
        private int  value;

        private Node next;
        private Node previous;


        public Node(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public Node getNext() {
            return next;
        }

        public Node getPrevious() {
            return previous;
        }

        public void setPrevious(Node previous) {
            this.previous = previous;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    Integer[] ar;
    private Node node;
    private Node last;

    public LinkedList2(Integer[] ar) {
        for (int num:ar) {
            add(num);
        }
    }

    private Node findNode(int key) {
        Node node1 = this.node;
        while (node1 != null) {
            if (node1.getValue() == key) {
                return node1;
            }
            node1 = node1.getNext();
        }
        return null;
    }
    @Override
    public void clear() {
        Node node1 = this.node;
        while (node1 != null) {
            Node node1Next = node1.getNext();
            node1.setNext(null);
            node1.setPrevious(null);
            node1 = node1Next;
        }
        this.last = null;
        this.node = null;
    }

    @Override
    public Object size() {
        Node node1 = this.node;
        int count = 0;
        while (node1 != null) {
            count++;
            node1 = node1.getNext();
        }
        return count;
    }

    @Override
    public Object get(int index) {
        return toArray()[index];
    }


    public boolean addFirst(int data) {
        int size=(int) size();
        int i = (int)size();
        Node node = new Node(data);
        if (this.node == null) {
            this.node = node;
            this.last = node;
        } else {
            node.setNext(this.node);
            node.setPrevious(node);
            node = node;
        }
        if (size - i == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean addLast(int data) {
        Node node = new Node(data);
        int size=(int) size();
        int i = (int)size();
        if (this.node == null) {
            this.node = node;
            this.last = node;
        } else {
            last.setNext(node);
            node.setPrevious(last);
            this.last = node;
        }
        if (size - i == 1) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public boolean add(Object value) {
        int size=(int) size();
        int i = (int)size();
        addLast((Integer) value);
        if (size - i == 1) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public boolean add(int index, Object value) {
        int size=(int) size();
        if (index < 0 || index > size) {
            return false;
        }
        if (index == 0) {
            addFirst((int)value);
            return false;
        }
        if (index == size) {
            addLast((int)value);
            return false;
        }
        int i =(int) size();
        Node node1 = this.node;
        while (index != 0) {
            node1 = node1.getNext();
            index--;
        }
        Node node = new Node((int)value);
        node.setNext(node1);
        node.setPrevious(node1.getPrevious());
        node.getPrevious().setNext(node);
        node1.setPrevious(node);
        if (size - i == 1) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public boolean remove(Object value) {
        Node node1 = this.findNode((Integer) value);
        int size=(int) size();
        int i = (int)size();
        if (node1 == null) {
            return false;
        }
        if (node1 == this.node && size == 1) {
            clear();
            return true;
        }
        if (node1 == this.node && size != 1) {
            this.node = this.node.getNext();
            this.node.setPrevious(null);
            if (size - i == -1) {
                return true;
            } else {
                return false;
            }
        }
        if (node1 == this.last) {
            node1.getPrevious().setNext(null);
            this.last = this.last.getPrevious();
            return false;
        }
        node1.getPrevious().setNext(node1.getNext());
        node1.getNext().setPrevious(node1.getPrevious());
        if ((int)size() - i == -1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object removeByIndex(int index) {
        int size=(int) size();
        if (index > size){
            return -1;
        }
        else {
            Object num = toArray()[index];
            remove(num);
            return index;
        }
    }
    @Override
    public boolean contains(Object value) {
        Node node1 = this.node;
        while (node1 != null) {
            if (node1.getValue() == (int)value) {
                return true;
            }
            node1 = node1.getNext();
        }
        return false;
    }

    @Override
    public boolean set(int index, Object value) {
        int size=(int) size();
        if ((int)index > size) {
            return false;
        }
        else {
            Object[] ar = toArray();
            ar[(int) index] =(int) value;
            clear();
            for (int i = 0; i < ar.length; i++) {
                add(ar[i]);
            }
            if ((int)toArray()[index] ==(int) value) {
                return true;
            } else {
                return false;
            }
        }
    }


    @Override
    public String print() {
        int size =(int) size();
        StringBuilder str = new StringBuilder();
        if (size == 0) {
            return str.toString();
        }
        Node tmp = this.node;
        while (tmp != null) {
            str.append(tmp.value).append(", ");
            tmp = tmp.next;
        }
        str.deleteCharAt(str.length() - 1);
        return str.toString();
    }

    @Override
    public Object[] toArray() {
        Node node1 = this.node;
        Object[] ar = new Object[(int) size()];
        int i = 0;
        while (node1 != null) {
            ar[i] = node1.getValue();
            node1 = node1.getNext();
            i++;
        }
        return ar;
    }


    @Override
    public boolean removeAll(Object[] ar) {
        if (toArray() == ar){
            clear();
            return true;
        }
        int num = (int) size();
        for (int i = 0; i < ar.length; i++) {
            remove(ar[i]);
        }
        if ((int)size() == num) {
            return false;
        } else {
            return true;
        }
    }

}
