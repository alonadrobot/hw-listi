package com.listi.LinkedList;

import com.listi.LinkedList.IList;

public class ArrayList2 implements IList {

        private int capacity;
        private Object[] array;

        public ArrayList2() {
            capacity = 10;
            array = new Integer[capacity];
        }

        public ArrayList2(int capacity) {
            this.capacity = capacity;
            array = new Integer[capacity];
        }

        public ArrayList2(Object[] array) {
            this.array = array;
            capacity = array.length;
        }

        @Override
        public void clear() {
            array = new Object[0];
        }

        @Override
        public Object size() {
            return array.length;
        }

        @Override
        public Object get(int index) {
            return array[index];
        }

        @Override
        public boolean add(Object value) {
            int amount = 0;
            for (int i = 0; i < array.length; i++) {
                if((array[i]+"").equals("null")){
                    array[i] = (int)value;
                    amount++;
                    return true;
                }
            }
            if (amount == 0){
                Object[] ar1 = new Object[array.length*2];
                for (int i = 0; i < array.length; i++){
                    ar1[i] = array[i];
                }
                ar1[array.length] = (int)value;
                array = ar1;
                return true;
            }
            return false;
        }

        @Override
        public boolean add(int index, Object value) {
            if(index < array.length){
                Object[] ar1 = new Object[array.length+1];
                for(int i = 0; i < index; i++){
                    ar1[i] = array[i];
                }
                ar1[index] = value;
                for(int i = index+1; i < array.length+1; i++){
                    ar1[i] = array[i-1];
                }
                array = ar1;
                return true;
            }
                return false;
        }

        @Override
        public boolean remove(Object number) {
            int index = -1;

            for(int i = 0; i < array.length; i++){
                if(array[i] == number){
                    index = i;
                    break;
                }
            }

            if(index == -1){
                return false;
            }
            else {
                Object[] ar1 = new Object[array.length+1];
                for (int i = 0; i < index; i++){
                    ar1[i] = array[i];
                }
                for (int i = index+1; i < array.length; i++){
                    ar1[i-1] = array[i];
                }
                array = ar1;
                return true;
            }
        }

        @Override
        public Object removeByIndex(int index) {
            if(index >= array.length){
                return -1;
            }
            else {
                Object[] ar1 = new Object[array.length+1];
                for (int i = 0; i < index; i++){
                    ar1[i] = array[i];
                }
                for (int i = index+1; i < array.length; i++){
                    ar1[i-1] = array[i];
                }
                array = ar1;
                return index;
            }
        }

        @Override
        public boolean contains(Object value) {
            for (Object num:array) {
                if (num == null){
                    continue;
                }
                else {
                    if(num == value){
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public boolean set(int index, Object value) {
            if(index < array.length){
                array[index] = (int)value;
                return true;
            }
            return false;
        }

        @Override
        public String print() {
            int size = (int)size();
            StringBuilder str = new StringBuilder();
            if (size == 0){
                return str.toString();
            }
            for (int i = 0; i < size - 1; i++) {
                str.append(this.array[i]).append(", ");
            }
            str.append(this.array[size - 1]);
            return str.toString();
        }

        @Override
        public Object[] toArray() {
            Object[] ar1 = new Object[array.length];
            for(int i = 0; i < array.length; i++){
                ar1[i] = array[i];
            }
            return ar1;
        }

        @Override
        public boolean removeAll(Object[] ar) {
            Object[] arr = array;
            for(int i = 0; i < ar.length; i++){
                remove(ar[i]);
            }

            if(array != arr){
                return true;
            }
            return false;
        }
    }

