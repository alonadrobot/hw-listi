package com.listi.LinkedList;

import com.listi.LinkedList.IList;

public class  LinkedList1 implements IList {
        class Node {
            Object data;
            Node next;

            public Node(Object data) {
                this.data = data;
            }
        }

        Object[] ar;
        Node node;


        public LinkedList1(Object[] ar) {
            for (Object number : ar) {
                add(number);
            }
        }

        public void clear() {
            node = null;
        }

        public Object size() {
            int i = 0;
            Node current = node;
            while (current != null) {
                i += 1;
                current = current.next;
            }
            return i;
        }

        @Override
        public Object get(int index) {
            Object[] arr = toArray();
            return arr[index];
        }
        @Override
        public boolean add(Object value) {
            int size = (int)size();
            if (node == null) {
                node = new Node((Integer) value);
                return true;
            }

            Node current = node;
            while (current.next != null) {
                current = current.next;
            }
            current.next = new Node((Integer) value);
            int size1 = (int)size();
            if (size1 == size) {
                return false;
            } else {
                return true;
            }
        }

        @Override
        public boolean add(int index, Object value) {
            Object[] arr = toArray();
            if (index >= arr.length) {
                return false;
            } else {
                clear();
                for (int i = 0; i < index; i++) {
                    add(arr[i]);
                }
                add(value);
                for (int i = index; i < arr.length; i++) {
                    add(arr[i]);
                }
                return true;
            }


        }

        private Object getIndex(Object value, Object[] arr) {
            int index = -1;
            for (int i = 0; i < arr.length; i++) {
                if (value == arr[i]) {
                    index = i;
                }
            }
            return index;
        }

        @Override
        public boolean remove(Object value) {
            Object[] arr = toArray();
            if (contains(value)) {
                removeByIndex((Integer) getIndex(value, arr));
                return true;
            } else {
                return false;
            }
        }

        @Override
        public Object removeByIndex(int index) {
            Object[] arr = toArray();
            if (index < arr.length) {
                clear();
                for (int i = 0; i < arr.length; i++) {
                    if (i == index) {
                        continue;
                    }
                    add(arr[i]);
                }
                return index;
            } else {
                return -1;
            }

        }

        @Override
        public boolean contains(Object value) {
            Object[] arr = toArray();
            for (Object number : arr) {
                if (number == value) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean set(int index, Object value) {
            Object[] arr = toArray();
            if (index >= arr.length) {
                return false;
            } else {
                clear();
                for (int i = 0; i < index; i++) {
                    add(arr[i]);
                }
                add(value);
                for (int i = index + 1; i < arr.length; i++) {
                    add(arr[i]);
                }
                return true;
            }
        }
        @Override
        public String print() {
            Node current = node;
            if (current == null) {
                System.out.println("[]");
            } else {
                System.out.print("[");
                while (current.next != null) {
                    System.out.print(current.data + ", ");
                    current = current.next;
                }
                System.out.println(current.data + "]");
            }
            return "";
        }

            @Override
            public Object[] toArray () {
                int size=(int) size();
                Object[] Arr;
                Node current = node;
                if (current == null) {
                    Arr = new Object[0];
                } else {
                    Object[] helpArr = new Object[size];
                    int iterator = 0;
                    for (int i = 0; i < size; i++) {
                        helpArr[iterator] = current.data;
                        current = current.next;
                        iterator++;
                    }
                    Arr = helpArr;
                }
                return Arr;
            }

            @Override
            public boolean removeAll ( Object [] ar){
                Object[] arr = toArray();
                boolean contain = false;

                for (Object size : ar) {
                    for (Object size1 : arr) {
                        if (size == size1) {
                            contain = true;
                            break;
                        }
                    }
                }

                if (contain) {
                    for (Object number : ar) {
                        remove(number);
                    }
                    return true;
                } else {
                    return false;
                }
            }


        }
